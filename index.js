const { createHash } = require('crypto');
const { existsSync, lstatSync, mkdirSync, readFileSync, writeFileSync } = require('fs');
const { dirname, join, relative, resolve } = require('path');

const { CLIEngine } = require('eslint');
const yaml = require('js-yaml');

const {
  CI_CONFIG_PATH = '.gitlab-ci.yml',
  CI_JOB_NAME,
  CI_PROJECT_DIR = process.cwd(),
  ESLINT_CODE_QUALITY_REPORT,
  ESLINT_CODE_QUALITY_REPORT_WARNING_SEVERITY,
  ESLINT_CODE_QUALITY_REPORT_ERROR_SEVERITY,
  ESLINT_FORMATTER,
} = process.env;

/**
 * @returns {string} The output path of the code quality artifact.
 */
function getOutputPath() {
  const configPath = join(CI_PROJECT_DIR, CI_CONFIG_PATH);
  // GitlabCI allows a custom configuration path which can be a URL or a path relative to another
  // project. In these cases CI_CONFIG_PATH is empty and we'll have to require the user provide
  // ESLINT_CODE_QUALITY_REPORT.
  if (!existsSync(configPath) || !lstatSync(configPath).isFile()) {
    throw new Error(
      'Could not resolve .gitlab-ci.yml to automatically detect report artifact path.' +
        ' Please manually provide a path via the ESLINT_CODE_QUALITY_REPORT variable.',
    );
  }
  const jobs = yaml.safeLoad(readFileSync(configPath, 'utf-8'));
  const { artifacts } = jobs[CI_JOB_NAME];
  const location = artifacts && artifacts.reports && artifacts.reports.codequality;
  const msg = `Expected ${CI_JOB_NAME}.artifacts.reports.codequality to be one exact path`;
  if (!location) {
    throw new Error(`${msg}, but no value was found.`);
  }
  if (Array.isArray(location)) {
    throw new TypeError(`${msg}, but found an array instead.`);
  }
  return resolve(CI_PROJECT_DIR, location);
}

/**
 * @param {string} filePath - The path to the linted file.
 * @param {object} message - The ESLint report message.
 * @returns {string} The fingerprint for the ESLint report message.
 */
function createFingerprint(filePath, message) {
  const md5 = createHash('md5');
  md5.update(filePath);
  if (message.ruleId) {
    md5.update(message.ruleId);
  }
  md5.update(message.message);
  return md5.digest('hex');
}

/**
 * @param {object[]} results - The ESLint report results.
 * @returns {object[]} The ESLint messages in the form of a GitLab code quality report.
 */
function convert(results) {
  const warningSeverity = ESLINT_CODE_QUALITY_REPORT_WARNING_SEVERITY || 'minor';
  const errorSeverity = ESLINT_CODE_QUALITY_REPORT_ERROR_SEVERITY || 'major';
  return results.reduce(
    (acc, result) => [
      ...acc,
      ...result.messages.map((message) => {
        const relativePath = relative(CI_PROJECT_DIR, result.filePath);
        // https://github.com/codeclimate/spec/blob/master/SPEC.md#data-types
        return {
          description: message.message,
          severity: message.severity === 2 ? errorSeverity : warningSeverity,
          fingerprint: createFingerprint(relativePath, message),
          location: {
            path: relativePath,
            lines: {
              begin: message.line,
            },
          },
        };
      }),
    ],
    [],
  );
}

module.exports = (results) => {
  if (CI_JOB_NAME || ESLINT_CODE_QUALITY_REPORT) {
    const data = convert(results);
    const outputPath = ESLINT_CODE_QUALITY_REPORT || getOutputPath();
    const dir = dirname(outputPath);
    mkdirSync(dir, { recursive: true });
    writeFileSync(outputPath, JSON.stringify(data, null, 2));
  }
  let formatter = CLIEngine.getFormatter(ESLINT_FORMATTER);
  if (formatter === module.exports) {
    formatter = CLIEngine.getFormatter();
  }
  return formatter(results);
};
